package SR3.Narath;
import java.util.List;
import java.util.Scanner;
import java.util.Iterator;

public class Remove {
    Scanner cin=new Scanner(System.in);
    public void RemoveList(List<Stock> object) {
        int id = 0, count = 0;
        System.out.println("\n" + "================ DELETE STAFF MEMBER ================");
        //    use exception handling for validate id
        do {
            try {
                System.out.print("=> Enter ID to Delete  :  ");
                id = cin.nextInt();
                count = 0;
                if (id < 0)
                    throw new Exception();
            } catch (Exception e) {
                System.out.println("-> INPUT IS INVALID ! (Please Input Integer type)....");
                count++;
            }
            cin.nextLine();
        } while (count != 0);
        //use interator instead loop to avoid concurrentmodification exception
        Iterator<Stock> it = object.iterator();
        while (it.hasNext()) {
            Stock tmp = it.next();
            //  if user input id equal any id of our collection then remove it
            if (id == tmp.getId()) {
                //  call tostring for display data
                System.out.println(tmp.toString());
                it.remove();
                System.out.println("=> Remove Successfully !");
                count++;
            }
        }
        if (count == 0)
            System.out.println("=> ID : " + id + " IS NOT EXIST...");
    }
}
