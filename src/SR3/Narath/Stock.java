package SR3.Narath;

import SR3.Regex;

import java.time.LocalDate;
import java.util.Scanner;

public class Stock {
    private int id,qty;
    private double unitprice;
    private String name;
    Stock(int id,String name,double unitprice,int qty){
        this.id=id;
        this.name=name;
        this.unitprice=unitprice;
        this.qty=qty;
    }
    Stock(){}
    public static Scanner cin = new Scanner(System.in);
    public void AddnewStock() {

        int count=0;
        do {
            try {
                System.out.print("=> Enter Staff Member's ID      :  ");
                id = cin.nextInt();
                count = 0;
                if (id < 0)
                    throw new Exception();
            } catch (Exception e) {
                System.out.println("-> INPUT IS INVALID ! (Please Input Integer type)....");
                count++;
            }
            cin.nextLine();
        } while (count != 0);
        do {
            System.out.print("=> Enter Product Name    :  ");
            name = cin.nextLine();
            if (Regex.onlystring(name))
                System.out.println("-> INPUT IS INVALID ! (Please Input String type)....");
        } while (Regex.onlystring(name));
        do {
            try {
                System.out.print("=> Enter UnitPrice  :  ");
                unitprice = cin.nextDouble();
                count = 0;
                if (unitprice < 0) {
                    throw new Exception();
                }
            } catch (Exception e) {
                System.out.println("-> INPUT IS INVALID ! (Please Input Double type)....");
                count++;
            }
            cin.nextLine();
        } while (count != 0);
        do {
            try {
                System.out.print("=> Enter QTY      :  ");
                qty = cin.nextInt();
                count = 0;
                if (qty < 0)
                    throw new Exception();
            } catch (Exception e) {
                System.out.println("-> INPUT IS INVALID ! (Please Input Integer type)....");
                count++;
            }
            cin.nextLine();
        } while (count != 0);
    }

    public void Updatename(){
        do {
            System.out.print("=> Enter Product Name    :  ");
            name = cin.nextLine();
            if (Regex.onlystring(name))
                System.out.println("-> INPUT IS INVALID ! (Please Input String type)....");
        } while (Regex.onlystring(name));
    }
    public int getId() {
        return id;
    }

    public int getQty() {
        return qty;
    }

    public double getUnitprice() {
        return unitprice;
    }

    public String getName() {
        return name;
    }

    public LocalDate getMyObj() {
        return myObj;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public void setUnitprice(double unitprice) {
        this.unitprice = unitprice;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setMyObj(LocalDate myObj) {
        this.myObj = myObj;
    }

    LocalDate myObj = LocalDate.now();


    @Override
    public String toString() {
        return "\n"+"id : "+getId()+"\n"+"Name : "+getName()+"\n"+"Unitprice : "+getUnitprice()+"\n"+"qty : "+getQty()+"\n"+"Import Date : "+myObj+"\n"+"-------------------------";
    }
}
